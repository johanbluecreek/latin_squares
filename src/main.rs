
use std::{collections::HashSet, time::Duration};

use nalgebra::Matrix5;
use rand::{thread_rng, Rng};
use bevy::prelude::*;

/******************************
TODO
 * Make pressing empty button spawn
   list of symbols the use can select
   for that button
   - Make some GridButtons locked, such that no spawnbox is spawned
   - Lock the "?" box too
 * Add timer
 * Add summary/replay screen
******************************/

// TYPE ALIASES

type PrimGrid = Matrix5<usize>;

// GAME STATES

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
enum GameState {
    Startup,
    Game,
    End,
}

// COMPONENTS

#[derive(Component)]
struct Locked(bool);

#[derive(Component, PartialEq, Eq, Clone)]
struct TargetCoord(usize, usize);

impl TargetCoord {
    fn switch(&mut self, &r: &usize, &c: &usize) {
        self.0 = r;
        self.1 = c;
    }
}

#[derive(Component)]
struct SpawnButton(usize);

#[derive(Component)]
struct SelectionBox;

#[derive(Component)]
struct GridButton((usize, usize));

#[derive(Component)]
struct ScoreText;

#[derive(Component)]
struct GameTimerText;

#[derive(Component)]
struct CurrentLevel;

#[derive(Component)]
struct AnswerButtons(usize);

// RESOURCES

#[derive(Resource)]
enum RenameGrid {
    Grid(usize, (usize, usize)),
    None,
}

impl RenameGrid {
    fn to_none(&mut self) {
        *self = RenameGrid::None;
    }
    fn set_grid(&mut self, i: &usize, r: &usize, c: &usize) {
        *self = RenameGrid::Grid(*i, (*r, *c))
    }
}

#[derive(Resource)]
struct GameTimer {
    timer: Timer,
}

impl GameTimer {
    fn new() -> GameTimer {
        GameTimer {
            timer: Timer::new(Duration::from_secs(6*60), TimerMode::Once),
        }
    }
}

#[derive(Resource, Clone, PartialEq, Eq)]
struct Score {
    total: usize,
    correct: usize,
}

impl Score {
    fn new() -> Score {
        Score {
            total: 0,
            correct: 0,
        }
    }
    fn reset(&mut self) {
        self.total = 0;
        self.correct = 0;
    }
    fn is_zero(&self) -> bool {
        if self.total == 0 {
            return true
        }
        return false
    }
    fn is_failure(&self) -> bool {
        if self.correct == 0 {
            return true
        }
        return false
    }
}

#[derive(Resource, PartialEq, Eq)]
struct BestScore {
    best_ratio: Score,
    best_quantity: Score,
}

impl BestScore {
    fn new() -> BestScore {
        BestScore {
            best_ratio: Score::new(),
            best_quantity: Score::new()
        }
    }
    fn is_zero(&self) -> bool {
        if self.best_quantity.is_zero() && self.best_ratio.is_zero() {
            return true
        }
        return false
    }
    fn is_same(&self) -> bool {
        if self.best_quantity == self.best_ratio {
            return true
        }
        return false
    }
    fn update(&mut self, score: &Score) {
        if score.is_zero() || score.is_failure() {
            return ()
        }
        if self.is_zero() {
            self.best_quantity = score.clone();
            self.best_ratio = score.clone();
            return ()
        }
        if score.correct > self.best_quantity.correct {
            self.best_quantity = score.clone();
        }
        if (score.correct as f32)/(score.total as f32) > (self.best_ratio.correct as f32)/(self.best_ratio.total as f32) {
            self.best_ratio = score.clone();
        }
    }
}

#[derive(Resource)]
struct FontHandle(pub Handle<Font>);

trait LatinPrimitive {
    fn promote(&self) -> Grid;
    fn promote_sol(&self, sol: usize) -> Grid;
}

impl LatinPrimitive for PrimGrid {
    fn promote(&self) -> Grid {
        return Grid {
            grid: self.clone(),
            solution: Solution::new(),
            mapping: Mapping::new(),
        }
    }
    fn promote_sol(&self, sol:usize) -> Grid {
        return Grid {
            grid: self.clone(),
            solution: Solution::from_usize(sol),
            mapping: Mapping::new(),
        }
    }
}

#[derive(Resource, Clone)]
struct Mapping {
    map: [String; 5]
}

trait SymbolArray {
    fn new() -> Mapping;
    fn get_text(&self, num: &usize, font: Handle<Font>) -> TextBundle;
}

impl SymbolArray for Mapping {
    fn new() -> Mapping {
        return Mapping {map: [
            "✚".to_string(),
            "●".to_string(),
            "▲".to_string(),
            "■".to_string(),
            "✸".to_string()
        ]}
    }
    fn get_text(&self, num: &usize, font: Handle<Font>) -> TextBundle {
        match num {
            5 => {
                TextBundle {
                    text: Text::from_section(" ",
                        TextStyle {
                            font: font,
                            font_size: 42.0,
                            color: Color::BLACK,
                        }
                    ),
                    ..default()
                }
            },
            6 => {
                TextBundle {
                    text: Text::from_section("?",
                        TextStyle {
                            font: font,
                            font_size: 42.0,
                            color: Color::WHITE,
                        }
                    ),
                    ..default()
                }
            },
            i => {
                TextBundle {
                    text: Text::from_section(self.map[*i].as_str(),
                        TextStyle {
                            font: font,
                            font_size: 42.0,
                            color: match self.map[*i].as_str() {
                                "✚" => Color::BLUE,
                                "●" => Color::GREEN,
                                "▲" => Color::ORANGE,
                                "■" => Color::RED,
                                "✸" => Color::PINK,
                                _ => Color::BLACK,
                            },
                        }
                    ),
                    ..default()
                }
            }
        }
    }
}

#[derive(Resource,Clone)]
struct Grid {
    grid: PrimGrid,
    solution: Solution,
    mapping: Mapping
}

trait LatinSquare {
    fn new_random() -> Grid;
    fn from_class() -> Grid;
    fn from_examples() -> Grid;
    fn new_example(&mut self);
    fn permute_random_row_pair_mut(&mut self);
    fn permute_random_numbers_mut(&mut self);
    fn scramble_mut(&mut self);
    fn get_proper_matching(&self) -> Vec<usize>;
    fn print(&self);
}

impl LatinSquare for Grid {
    fn new_random() -> Grid {
        let mut g = Grid::from_class();
        g.scramble_mut();
        return g;
    }
    /// The 5x5 grid has two "main classes", this selects a random one
    fn from_class() -> Grid {
        let mut rng = thread_rng();
        if rng.gen::<f64>() > 0.5 {
            return PrimGrid::new(
                0,1,2,3,4,
                1,2,4,0,3,
                2,4,3,1,0,
                3,0,1,4,2,
                4,3,0,2,1,
            ).promote()
        } else {
            return PrimGrid::new(
                0,1,2,3,4,
                1,3,0,4,2,
                2,4,3,1,0,
                3,0,4,2,1,
                4,2,1,0,3,
            ).promote()
        }
    }
    fn from_examples() -> Grid {
        let selection = thread_rng().gen_range(0..9);
        let mut grid = match selection {
            1 => {
                PrimGrid::new(
                    5,5,2,4,5,
                    5,6,5,5,5,
                    2,3,5,5,5,
                    5,5,4,1,3,
                    5,1,5,5,5,
                ).promote_sol(4)
            }
            2 => {
                PrimGrid::new(
                    5,5,2,5,0,
                    5,5,1,6,5,
                    5,5,4,3,5,
                    0,5,3,5,5,
                    5,2,5,5,5,
                ).promote_sol(0)
            }
            3 => {
                PrimGrid::new(
                    2,5,5,5,0,
                    5,4,0,5,5,
                    5,3,2,5,5,
                    5,5,6,3,5,
                    5,5,5,1,5,
                ).promote_sol(1)
            }
            4 => {
                PrimGrid::new(
                    5,5,3,5,5,
                    4,5,5,5,5,
                    5,0,4,2,1,
                    5,5,5,5,3,
                    5,6,5,0,5,
                ).promote_sol(3)
            }
            5 => {
                PrimGrid::new(
                    5,5,5,2,5,
                    5,5,3,5,0,
                    5,3,4,5,5,
                    5,6,1,5,5,
                    0,5,2,5,5,
                ).promote_sol(0)
            }
            6 => {
                PrimGrid::new(
                        5,5,5,5,1,
                        5,6,5,0,5,
                        4,5,5,3,5,
                        5,0,5,2,5,
                        5,5,4,1,5,
                    ).promote_sol(4)
            }
            7 => {
                PrimGrid::new(
                    5,5,5,1,5,
                    3,1,4,5,5,
                    5,5,5,3,2,
                    5,5,5,6,5,
                    5,4,2,5,5,
                ).promote_sol(4)
            }
            _ => {
                PrimGrid::new(
                    1,4,3,5,2,
                    5,5,5,5,5,
                    5,5,5,5,5,
                    6,3,2,5,1,
                    5,5,5,5,5,
                ).promote_sol(0)
            }
        };
        grid.scramble_mut();
        return grid
    }
    fn new_example(&mut self) {
        let new_grid = Grid::from_examples();
        self.grid = new_grid.grid;
        self.solution = new_grid.solution;
    }
    /// Permutes two random rows in-place
    fn permute_random_row_pair_mut(&mut self) {
        let mut rng = thread_rng();
        let r1: usize = rng.gen_range(0..5);
        let mut r2: usize = rng.gen_range(0..5);
        while r1 == r2 {
            r2 = rng.gen_range(0..5);
        }
        let g = self.grid.clone();
        let row1 = g.row(r1);
        let row2 = g.row(r2);
        self.grid.set_row(r1, &row2);
        self.grid.set_row(r2, &row1);
    }
    /// Permutes two random group of numbers in-place
    fn permute_random_numbers_mut(&mut self) {
        let mut rng = thread_rng();
        let n1: usize = rng.gen_range(0..5);
        let mut n2: usize = rng.gen_range(0..5);
        while n1 == n2 {
            n2 = rng.gen_range(0..5);
        }
        let g = self.grid.clone();
        self.grid = PrimGrid::from_fn(
            |r,c| {
                let e = g.index((r,c));
                if e == &n1 {
                    n2
                } else if e == &n2 {
                    n1
                } else {
                    *e
                }
            }
        );
        if self.solution.number == n1 {
            self.solution.number = n2
        } else if self.solution.number == n2 {
            self.solution.number = n1
        }
    }
    // Scrambles the grid using row & number permutations with transposes, in-place
    fn scramble_mut(&mut self) {
        let max = thread_rng().gen_range(10..100);
        for _ in 0..max {
            for _ in  0..10 {
                self.permute_random_row_pair_mut()
            }
            for _ in  0..10 {
                self.permute_random_numbers_mut();
            }
            self.grid.transpose_mut()
        }
    }
    /// Returns a vector with ordered column indices for a proper matching
    fn get_proper_matching(&self) -> Vec<usize> {
        let mut cs = vec![];
        let all: HashSet<usize> = HashSet::from([0,1,2,3,4]);
        for e1 in all.iter() {
            for e2 in all.difference(&HashSet::from([*e1])) {
                for e3 in all.difference(&HashSet::from([*e1, *e2])) {
                    for e4 in all.difference(&HashSet::from([*e1, *e2, *e3])) {
                        for e5 in all.difference(&HashSet::from([*e1, *e2, *e3, *e4])) {
                            let mut cs_tmp = vec![];
                            for (r, entry) in [e1,e2,e3,e4,e5].iter().enumerate() {
                                for (c, _e) in self.grid.row(r).iter().enumerate().filter(|(_c,e)| {e == entry}) {
                                    cs_tmp.push(c);
                                }
                            }
                            let a: HashSet<usize> = cs_tmp.clone().into_iter().collect();
                            if cs_tmp.len() == a.len() {
                                cs = cs_tmp
                            }
                        }
                    }
                }
            }
        }
        return cs
    }
    fn print(&self) {
        println!("---------------------");
        for row in self.grid.row_iter() {
            print!("|");
            for e in row.iter() {
                if e == &5 {
                    print!("   |");
                } else if e == &6 {
                    print!(" ? |")
                } else {
                    print!(" {} |", self.mapping.map[*e])
                }
            }
            println!("\n---------------------")
        }
    }
}

#[derive(Resource,Clone)]
struct Solution {
    number: usize,
}

impl Solution {
    fn new() -> Solution {
        Solution {
            number: 0
        }
    }
    fn from_usize(sol: usize) -> Solution {
        Solution {
            number: sol,
        }
    }
}

// SYSTEMS

fn insert_initial_grid(mut commands: Commands) {
    let grid = Grid::from_examples();
    commands.insert_resource(grid);
}

fn new_grid(
    mut grid: ResMut<Grid>,
) {
    grid.new_example();
}

fn font_assets(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    commands.insert_resource(
        FontHandle(asset_server.load("font.ttf"))
    );
}

fn setup_camera(
    mut commands: Commands,
) {
    commands.spawn(Camera2dBundle::new_with_far(1000.0));
}

fn enter_game(
    mut game_state: ResMut<State<GameState>>
) {
    game_state.set(GameState::Game).unwrap();
}

fn draw_grid(
    mut commands: Commands,
    fonts: Res<FontHandle>,
    grid: Res<Grid>,
    mapping: Res<Mapping>,
) {
    let background_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            flex_direction: FlexDirection::ColumnReverse,
            align_items: AlignItems::Center,
            ..default()
        },
        background_color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let row_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            flex_direction: FlexDirection::ColumnReverse,
            align_items: AlignItems::Center,
            ..default()
        },
        background_color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let column_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            flex_direction: FlexDirection::Row,
            align_items: AlignItems::Center,
            ..default()
        },
        background_color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let button_bundle = ButtonBundle {
        style: Style {
            size: Size::new(Val::Px(48.0), Val::Px(48.0)),
            // center button
            margin: UiRect::all(Val::Px(5.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            ..default()
        },
        background_color: Color::rgba(0.3, 0.3, 0.3, 0.8).into(),
        ..default()
    };

    let answer_row = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Px(15.0)),
            flex_direction: FlexDirection::Row,
            align_items: AlignItems::Center,
            ..default()
        },
        background_color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let selection_row = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Px(15.0)),
            flex_direction: FlexDirection::Row,
            align_items: AlignItems::Center,
            ..default()
        },
        background_color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        visibility: Visibility {is_visible: false,},
        ..default()
    };

    let answer_button_bundle = ButtonBundle {
        style: Style {
            size: Size::new(Val::Px(48.0), Val::Px(48.0)),
            // center button
            margin: UiRect::all(Val::Px(8.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            ..default()
        },
        background_color: Color::rgba(0.3, 0.3, 0.3, 0.8).into(),
        ..default()
    };

    commands
        .spawn(background_node)
        .with_children(|parent| {
            parent.spawn(answer_row)
                .with_children(|parent| {
                    for i in 0..5 {
                        parent.spawn(answer_button_bundle.clone())
                            .with_children(|parent| {
                                parent.spawn(
                                    mapping.get_text(&i, fonts.0.clone())
                                );
                            })
                            .insert(AnswerButtons(i));
                    }
                });
            parent.spawn(row_node)
                .with_children(|parent| {
                    for (r, row) in grid.grid.row_iter().enumerate() {
                        parent.spawn(column_node.clone())
                            .with_children(|parent| {
                                for (c, e) in row.iter().enumerate() {
                                    parent.spawn(button_bundle.clone())
                                        .with_children(|parent| {
                                            parent.spawn(
                                                mapping.get_text(e, fonts.0.clone())
                                            );
                                        })
                                        .insert(GridButton((r, c)))
                                        .insert(
                                            Locked(
                                                match e {
                                                    &5 => {false},
                                                    _ => {true}
                                                }
                                            )
                                        );
                                }
                            });
                    }
                });
            parent.spawn(selection_row)
                .with_children(|parent| {
                    for i in 0..5 {
                        parent.spawn(answer_button_bundle.clone())
                            .with_children(|parent| {
                                parent.spawn(
                                    mapping.get_text(&i, fonts.0.clone())
                                );
                            })
                            .insert(SpawnButton(i))
                            .insert(TargetCoord(0,0));
                    }
                })
                .insert(SelectionBox);
        })
        .insert(CurrentLevel);
}

fn answer_button_interaction(
    interaction_query: Query<(&Interaction, &AnswerButtons),(Changed<Interaction>, With<AnswerButtons>)>,
    grid: Res<Grid>,
    mut score: ResMut<Score>,
    mut game_state: ResMut<State<GameState>>,
) {
    for (interaction, button) in interaction_query.iter() {
        match interaction {
            Interaction::Clicked => {
                score.total += 1;
                let AnswerButtons(i) = button;
                if &grid.solution.number == i {
                    score.correct += 1;
                }
                game_state.set(GameState::End).unwrap();
            },
            _ => {}
        }
    }
}

fn grid_button_interaction(
    interaction_query: Query<(&Interaction, &GridButton, &Locked),(Changed<Interaction>, With<GridButton>)>,
    mut selection_box_query: Query<&mut Visibility, With<SelectionBox>>,
    mut selection_button_query: Query<&mut TargetCoord, With<SpawnButton>>,
) {
    for (interaction, button, locked) in interaction_query.iter() {
        match interaction {
            Interaction::Clicked => {
                if !locked.0 {
                    let GridButton((r,c)) = button;
                    let mut vis = selection_box_query.single_mut();
                    if !vis.is_visible {
                        vis.toggle()
                    } else if selection_button_query.iter().any(|tc| {tc == &TargetCoord(*r,*c)}) {
                        vis.toggle()
                    }
                    for mut target_coord in selection_button_query.iter_mut() {
                        target_coord.switch(&r, &c);
                    }
                }
            },
            _ => {}
        }
    }
}

fn spawn_button_interaction(
    interaction_query: Query<(&Interaction, &SpawnButton, &TargetCoord), (Changed<Interaction>, With<SpawnButton>)>,
    mut rename: ResMut<RenameGrid>,
    mut selection_box_query: Query<&mut Visibility, With<SelectionBox>>,
) {
    if interaction_query.is_empty() {
        return ()
    }
    for (interaction, spawn_button, target_coord) in interaction_query.iter() {
        match interaction {
            Interaction::Clicked => {
                selection_box_query.single_mut().toggle();
                let SpawnButton(i) = spawn_button;
                let TargetCoord(r, c) = target_coord;
                rename.set_grid(i, r, c)
            },
            _ => {}
        }
    }
}

fn update_grid(
    mut rename: ResMut<RenameGrid>,
    button_query: Query<(&Children, &GridButton), With<GridButton>>,
    mut text_query: Query<&mut Text>,
    mapping: Res<Mapping>,
    fonts: Res<FontHandle>,
) {
    if let RenameGrid::Grid(i, (r,c)) = rename.as_ref() {
        for (children, button) in button_query.iter() {
            let GridButton((target_r, target_c)) = button;
            if target_r == r && target_c == c {
                let mut text = text_query.get_mut(children[0]).unwrap();
                let new_text = mapping.get_text(i, fonts.0.clone());
                text.sections[0].value = new_text.text.sections[0].value.clone();
                text.sections[0].style.color = new_text.text.sections[0].style.color;
            }
        }
        rename.to_none();
    }
}

fn despawn_current_level(
    mut commands: Commands,
    query: Query<Entity, With<CurrentLevel>>
) {
    for entity in query.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

fn setup_scoretext(
    mut commands: Commands,
    fonts: Res<FontHandle>,
) {
    commands.spawn(TextBundle {
            style: Style {
                position_type: PositionType::Absolute,
                position: UiRect {
                    top: Val::Px(8.0),
                    left: Val::Px(10.0),
                    ..default()
                },
                ..default()
            },
            text: Text {
                sections: vec![
                    TextSection {
                        value: "Score: ".to_string(),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: 24.0,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: "0/0".to_string(),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: 24.0,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: "".to_string(),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: 24.0,
                            color: Color::WHITE,
                        },
                    },
                ],
                ..default()
            },
            ..default()
        })
        .insert(ScoreText);
}

fn update_scoretext(
    mut query: Query<&mut Text, With<ScoreText>>,
    score: Res<Score>,
) {
    let mut text = query.single_mut();
    if score.is_zero() {
        text.sections[1].value = format!(
            "{}/{}"
        , score.correct, score.total);
    } else {
        text.sections[1].value = format!(
            "{}/{} ({:.1}%)"
        , score.correct, score.total, ((score.correct as f32)/(score.total as f32)*100.0));
    }
}

fn update_bestscoretext(
    mut query: Query<&mut Text, With<ScoreText>>,
    best_score: Res<BestScore>,
) {
    let mut text = query.single_mut();
    if !best_score.is_zero() {
        if best_score.is_same() {
            text.sections[2].value = format!(
                "\n\nBest score: {}/{} ({:.1}%)",
                best_score.best_ratio.correct,
                best_score.best_ratio.total,
                ((best_score.best_ratio.correct as f32)/(best_score.best_ratio.total as f32)*100.0)
            );
        } else {
            text.sections[2].value = format!(
                "\n\nBest score:\n    ratio: {}/{} ({:.1}%)\n quantity: {}/{} ({:.1}%)",
                best_score.best_ratio.correct,
                best_score.best_ratio.total,
                ((best_score.best_ratio.correct as f32)/(best_score.best_ratio.total as f32)*100.0),
                best_score.best_quantity.correct,
                best_score.best_quantity.total,
                ((best_score.best_quantity.correct as f32)/(best_score.best_quantity.total as f32)*100.0),
            );
        }
    }
}

fn update_gametimer(
    mut game_timer: ResMut<GameTimer>,
    time: Res<Time>,
) {
    game_timer.timer.tick(time.delta());
}

fn setup_gametimer_text(
    mut commands: Commands,
    fonts: Res<FontHandle>,
    game_timer: Res<GameTimer>,
) {
    let mut seconds = game_timer.timer.remaining_secs();
    let minutes = (seconds / 60.0).trunc();
    seconds = (seconds - minutes*60.0).trunc();
    commands.spawn(TextBundle {
            style: Style {
                position_type: PositionType::Absolute,
                position: UiRect {
                    top: Val::Px(8.0),
                    right: Val::Px(10.0),
                    ..default()
                },
                ..default()
            },
            text: Text {
                sections: vec![
                    TextSection {
                        value: "Time left: ".to_string(),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: 24.0,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: format!(
                            "{}:{}"
                        , minutes, seconds),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: 24.0,
                            color: Color::WHITE,
                        },
                    },
                ],
                ..default()
            },
            ..default()
        })
        .insert(GameTimerText);
}

fn update_gametimer_text(
    mut query: Query<&mut Text, With<GameTimerText>>,
    game_timer: Res<GameTimer>,
) {
    let mut text = query.single_mut();
    let mut seconds = game_timer.timer.remaining_secs();
    let minutes = (seconds / 60.0).trunc();
    seconds = (seconds - minutes*60.0).trunc();
    text.sections[1].value = format!(
        "{:0>2}:{:0>2}"
    , minutes as i32, seconds as i32);
}

fn handle_timeout(
    mut game_timer: ResMut<GameTimer>,
    mut game_state: ResMut<State<GameState>>,
    mut score: ResMut<Score>,
    mut best_score: ResMut<BestScore>,
) {
    if game_timer.timer.just_finished() {
        game_state.set(GameState::End).unwrap();
        game_timer.timer.reset();
        best_score.update(&score);
        score.reset();
    }
}

// MAIN

fn main() {
    App::new()
        .add_plugins(
            DefaultPlugins.set(
                WindowPlugin {
                    window: WindowDescriptor {
                        scale_factor_override: Some(1.0),
                        ..default()
                    },
                    ..default()
                }
            )
        )
        .add_state(GameState::Startup)
        .insert_resource(Score::new())
        .insert_resource(BestScore::new())
        .insert_resource(Mapping::new())
        .insert_resource(RenameGrid::None)
        .insert_resource(GameTimer::new())
        // Start-up
        .add_system_set(
            // System set for early loading
            SystemSet::on_enter(GameState::Startup)
                .with_system(insert_initial_grid)
                .with_system(font_assets)
                .with_system(setup_camera)
        )
        .add_system_set(
            // Transitional system set
            SystemSet::on_update(GameState::Startup)
                .with_system(enter_game)
        )
        .add_system_set(
            // System set for late loading
            SystemSet::on_exit(GameState::Startup)
                .with_system(setup_scoretext)
                .with_system(setup_gametimer_text)
        )
        // Game
        .add_system_set(
            // Game setup system set
            SystemSet::on_enter(GameState::Game)
                .with_system(draw_grid)
        )
        .add_system_set(
            SystemSet::on_update(GameState::Game)
                .with_system(answer_button_interaction)
                .with_system(grid_button_interaction)
                .with_system(spawn_button_interaction)
                .with_system(update_grid)
                .with_system(update_gametimer)
                .with_system(update_gametimer_text)
                .with_system(handle_timeout)
        )
        .add_system_set(
            SystemSet::on_exit(GameState::Game)
                .with_system(despawn_current_level)
                .with_system(update_scoretext)
        )
        .add_system_set(
            SystemSet::on_enter(GameState::End)
                .with_system(new_grid)
                .with_system(update_bestscoretext)
        )
        .add_system_set(
            SystemSet::on_update(GameState::End)
                .with_system(enter_game)
        )
        .run();
}
