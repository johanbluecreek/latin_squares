# Latin Squares

A little game for practicing 5x5 Latin square problems.


## TODO

 * ~~Add timer~~
 * Add menu
   - Start button and exit button
   - Menu should appear at start and when timer runs out
 * Add Settings menu-item
   - Possibility to change timer
   - Difficulty setting?
 * Save score
